package model;

public enum ModeType {
    EASY, MEDIUM, HARD, IMPOSSIBLE, TEST, MAIN
}
