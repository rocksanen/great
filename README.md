# Memory Game
Game where you flip cards to find pairs. Made with JavaFX.
## Table of Contents
* [Instructions](#instructions)
* [About The Project](#about-the-project)
* [Getting Started](#getting-started)

## Instructions
To get started, build the game in Intellij with Java version 17 or higher.

## About The Project
This project was made for ohjelmistotuotantoprojekti 1 course at Metropolia UAS.

## Getting Started
After building the game, you can select one of three difficulties and get started immediately. Database server not included in the repo.
